package com.example.apptonghop;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    Button btnsignin, btnsignup;
    FrameLayout frameLayout;
    ImageView fb, gg, ins;
    float v = 0;
    LoginTabFragment loginTabFragment = new LoginTabFragment();
    SignupTabFragment signupTabFragment = new SignupTabFragment();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_view, loginTabFragment).commit();

        btnsignin = findViewById(R.id.btnsignin);
        btnsignup = findViewById(R.id.btnsignup);

        btnsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_view, signupTabFragment).commit();
            }
        });
        btnsignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_view, loginTabFragment).commit();
            }
        });



        fb = findViewById(R.id.fab_facebook);
        gg = findViewById(R.id.fab_google);
        ins = findViewById(R.id.fab_ínstagram);

        fb.setTranslationY(300);
        gg.setTranslationY(300);
        ins.setTranslationY(300);


        gg.setAlpha(v);
        ins.setAlpha(v);
        fb.setAlpha(v);


        fb.animate().translationY(0).alpha(1).setDuration(1000).setStartDelay(400).start();
        gg.animate().translationY(0).alpha(1).setDuration(1000).setStartDelay(600).start();
        ins.animate().translationY(0).alpha(1).setDuration(1000).setStartDelay(800).start();



    }
}