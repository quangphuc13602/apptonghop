package com.example.apptonghop;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import java.util.ArrayList;
import java.util.List;

import category.Category;
import category.CategoryAdapter;
import product.Product;


public class FragmentListProduct extends Fragment {

    private RecyclerView rcvCategory;
    private CategoryAdapter categoryAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_product, container, false);

        rcvCategory = view.findViewById(R.id.rcv_category);
        categoryAdapter = new CategoryAdapter(getActivity());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        rcvCategory.setLayoutManager(linearLayoutManager);

        categoryAdapter.setData(getListCategory());
        rcvCategory.setAdapter(categoryAdapter);


        return view;
    }
    private List<Category> getListCategory(){
        List<Category> listCategory = new ArrayList<>();

        List<Product> productListnike = new ArrayList<>();
        productListnike.add(new Product(R.drawable.nikeairforce1low_paisley_swoosh, "Nike Air Force 1 Low Paisley Swoosh", "1.300.000 VND","42"));
        productListnike.add(new Product(R.drawable.jordan1, "Nike Jordan 1", "1.500.000 VND","42"));
        productListnike.add(new Product(R.drawable.nikeairforce1, "Nike Air Force 1", "2.000.000 VND","42"));
        productListnike.add(new Product(R.drawable.nikeairjordan1low, "Nike Jordan 1 Low", "2.350.000 VND","42"));
        productListnike.add(new Product(R.drawable.nikeair_uptempo_whitered, "Nike Air Uptempo White/Red", "3.499.000 VND","42"));

        List<Product> productListadidas = new ArrayList<>();
        productListadidas.add(new Product(R.drawable.adidas_alphabounce_mens_white, "Adidas Alphabounce Mens White", "3.500.000 VND","42"));
        productListadidas.add(new Product(R.drawable.adidas_nmd_v3, "Adidas NMD V3", "1.000.000 VND","42"));
        productListadidas.add(new Product(R.drawable.adidas_ultraboost, "Adidas UltraBoost", "2.700.000 VND","42"));
        productListadidas.add(new Product(R.drawable.superstar_ot_tech_shoes, "Superstar OT Tech Shoe", "3.400.000 VND","42"));
        productListadidas.add(new Product(R.drawable.adidas_yeezy_boost_350_v2_casual, "Adidas Yeezy Boost 350 V2 Casual", "1.360.000 VND","42"));

        List<Product> productListbasketball = new ArrayList<>();
        productListbasketball.add(new Product(R.drawable.adidas_harden_vol5_futurenatural_white, "Adidas Harden Vol.5 Futurenatural White", "12.000.000 VND","42"));
        productListbasketball.add(new Product(R.drawable.lebron_19, "Lebron 19", "17.000.000 VND","42"));
        productListbasketball.add(new Product(R.drawable.nike_lebron_17, "Lebron 17", "1.000.000 VND","42"));
        productListbasketball.add(new Product(R.drawable.nike_kyrie_6_concepts_khepri_regular_box, "Nike Kyrie 6 Concepts Khepri", "1.000.000 VND","42"));
        productListbasketball.add(new Product(R.drawable.nike_kyrie_7_ep_cq9327_600, "Nike Kyrie 7 Ep CQ9327", "1.000.000 VND","42"));

        listCategory.add(new Category("NIKE",productListnike));
        listCategory.add(new Category("ADIDAS",productListadidas));
        listCategory.add(new Category("BASKETBALL SNEAKER",productListbasketball));

        return listCategory;

    }
}