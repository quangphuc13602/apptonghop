package com.example.apptonghop;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.fragment.app.Fragment;



public class LoginTabFragment extends Fragment {

    EditText email, pass ;
    TextView forgetpass;
    Button login;
    float v = 0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle){
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.login_tab_fragment, viewGroup, false);


        email = root.findViewById(R.id.emaillogin);
        pass = root.findViewById(R.id.passlogin);
        forgetpass = root.findViewById(R.id.fgpass);
        login = root.findViewById(R.id.buttonlogin);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getText().toString().equals("")  && pass.getText().toString().equals("")) {
                    Intent i = new Intent(getActivity(), MainActivityHome.class);
                    startActivity(i);
                } else if (email.getText().toString().isEmpty() && pass.getText().toString().isEmpty()){
                    email.requestFocus();
                }
            }
        });


        email.setTranslationX(800);
        pass.setTranslationX(800);
        forgetpass.setTranslationX(800);
        login.setTranslationX(800);

        email.setAlpha(v);
        pass.setAlpha(v);
        forgetpass.setAlpha(v);
        login.setAlpha(v);

        email.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        pass.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        forgetpass.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(700).start();
        login.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(700).start();

        return root;
    }
}
