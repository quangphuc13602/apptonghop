package com.example.apptonghop;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import product.Product;

public class MainActivityDetailMall extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_detail);



        Bundle bundle = getIntent().getExtras();
        if(bundle == null){
            return;
        }
        Product product = (Product) bundle.get("object_product");

        TextView tvback = (TextView) findViewById(R.id.tv_back_detail);
        tvback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivityDetailMall.this, FragmentListMall.class);
                startActivity(intent);
            }
        });

        TextView tvname = (TextView) findViewById(R.id.tv_name_detail);
        TextView tvprice = (TextView) findViewById(R.id.tv_price_detail);
        TextView tvsize = (TextView) findViewById(R.id.tv_size_detail);
        ImageView imageView = (ImageView) findViewById(R.id.imageViewProduct);


        tvname.setText(product.getTitle());
        tvprice.setText(product.getPrice());
        tvsize.setText(product.getSize());
        imageView.setImageResource(product.getResourceid());

    }
}