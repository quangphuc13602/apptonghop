package com.example.apptonghop;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

public class SignupTabFragment extends Fragment {

    EditText email, pass, confpass ;
    Button signup;
    float v=0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle bundle){
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.signup_tab_fragment, viewGroup, false);

        email = root.findViewById(R.id.emailsignup);
        pass = root.findViewById(R.id.passsignup);
        confpass = root.findViewById(R.id.confpass);
        signup = root.findViewById(R.id.buttonsignup);

        email.setTranslationX(800);
        pass.setTranslationX(800);
        confpass.setTranslationX(800);
        signup.setTranslationX(800);

        email.setAlpha(v);
        pass.setAlpha(v);
        confpass.setAlpha(v);
        signup.setAlpha(v);

        email.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        pass.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();
        confpass.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(700).start();
        signup.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(700).start();
        return root;
    }
}
