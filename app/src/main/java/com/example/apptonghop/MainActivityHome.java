package com.example.apptonghop;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.MenuItem;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

public class MainActivityHome extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    FragmentListProduct fragmentListProduct = new FragmentListProduct();
    FragmentListMall fragmentListMall = new FragmentListMall();
    FragmentProfile fragmentProfile = new FragmentProfile();

    @SuppressLint("SuspiciousIndentation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_home);

        bottomNavigationView = findViewById(R.id.bottom_navigation);
             getSupportFragmentManager().beginTransaction().replace(R.id.frame_view_home, fragmentListProduct).commit();

        bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.homepage:
                            getSupportFragmentManager().beginTransaction().replace(R.id.frame_view_home, fragmentListProduct).commit();
                        return true;
                    case R.id.mallpage:
                        getSupportFragmentManager().beginTransaction().replace(R.id.frame_view_home, fragmentListMall).commit();
                        return true;
                    case R.id.profilepage:
                        getSupportFragmentManager().beginTransaction().replace(R.id.frame_view_home, fragmentProfile).commit();
                        return true;
                }
                return false;
            }
        });
    }
}
