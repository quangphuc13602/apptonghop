package product;

import java.io.Serializable;

public class Product implements Serializable {
    private int resourceid;
    private String title;
    private String price;
    private String size;

    public Product(int resourceid, String title, String price, String size) {
        this.resourceid = resourceid;
        this.title = title;
        this.price = price;
        this.size = size;
    }



    public int getResourceid() {
        return resourceid;
    }

    public void setResourceid(int resourceid) {
        this.resourceid = resourceid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
